package module01

// BaseToDec takes in a number and the base it is currently
// in and returns the decimal equivalent as an integer.
//
// Eg:
//
//   BaseToDec('E', 16) => 14
//   BaseToDec('1110', 2) => 14
//
func BaseToDec(value string, base int) int {
	chars := map[byte]int{'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6,
		'7': 7, '8': 8, '9': 9, 'A': 10, 'B': 11, 'C': 12, 'D': 13, 'E': 14, 'F': 15}
	val := 0
	factor := 1
	for i := range value {
		rightMostChar := value[len(value)-1-i]
		val += chars[rightMostChar] * factor
		factor *= base
	}

	return val
}

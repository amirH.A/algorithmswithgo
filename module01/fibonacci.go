package module01

var fibonacciMemo map[int]int = map[int]int{0: 0, 1: 1}

// Fibonacci returns the nth fibonacci number.
//
// A Fibonacci number N is defined as:
//
//   Fibonacci(N) = Fibonacci(N-1) + Fibonacci(N-2)
//
// Where the following base cases are used:
//
//   Fibonacci(0) => 0
//   Fibonacci(1) => 1
//
//
// Examples:
//
//   Fibonacci(0) => 0
//   Fibonacci(1) => 1
//   Fibonacci(2) => 1
//   Fibonacci(3) => 2
//   Fibonacci(4) => 3
//   Fibonacci(5) => 5
//   Fibonacci(6) => 8
//   Fibonacci(7) => 13
//   Fibonacci(14) => 377
//
// Note: this solution uses memoization
func Fibonacci(n int) int {
	fib, ok := fibonacciMemo[n]
	if ok {
		return fib
	}

	fib1, ok := fibonacciMemo[n-1]
	if !ok {
		fib1 = Fibonacci(n - 1)
	}

	fib2, ok := fibonacciMemo[n-2]
	if !ok {
		fib2 = Fibonacci(n - 2)
	}

	fib = fib1 + fib2
	fibonacciMemo[n] = fib
	return fib
}
